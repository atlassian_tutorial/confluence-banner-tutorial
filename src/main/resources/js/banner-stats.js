AJS.toInit(function ($) {
    var dataLoaded = false; // only load inline dialog contents once
    var dialogId = "banner-stats-dialog";
    var $webItem = $('#banner-stats');

    var dialog = AJS.InlineDialog($webItem, dialogId,
                function(content, trigger, showPopup) {
                    if(!dataLoaded) {
                        content.html(Confluence.Templates.Plugins.Banner.Stats.loading());
                        content.find(".spinner").spin("medium");

                        $.ajax({
                            url: AJS.contextPath() + "/rest/banner-stats/1.0/banner-stats?pageId=" + AJS.Meta.get("page-id"),
                            type: "GET",
                            dataType: "json",
                            contentType: "application/json",
                            error:function () {
                                content.html(Confluence.Templates.Plugins.Banner.Stats.error());
                            },
                            success: function (response) {
                                response.pageId = AJS.Meta.get("page-id");
                                content.html(Confluence.Templates.Plugins.Banner.Stats.stats(response));
                                dataLoaded = true;
                            }
                        });
                    }

                    showPopup();
                    return false;
                }
            );

    // Workaround to dismiss the inline dialog when clicking on web-item again until https://ecosystem.atlassian.net/browse/AUI-1175 is done
    $webItem.click(function() {
        if($('#inline-dialog-' + dialogId).is(':visible')) {
            dialog.hide();
        }
    });
});