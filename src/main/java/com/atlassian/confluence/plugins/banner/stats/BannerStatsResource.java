package com.atlassian.confluence.plugins.banner.stats;

import com.atlassian.confluence.like.LikeManager;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.PageManager;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path ("banner-stats")
@Produces ({ MediaType.APPLICATION_JSON})
public class BannerStatsResource
{
    private LikeManager likeManager;
    private PageManager pageManager;

    public BannerStatsResource(final LikeManager likeManager, final PageManager pageManager)
    {
        this.likeManager = likeManager;
        this.pageManager = pageManager;
    }

    @GET
    public Response getMetadata(@QueryParam ("pageId") long pageId)
    {
        try
        {
            return Response.ok(getStats(pageId)).build();
        }
        catch (Exception e)
        {
            return Response.serverError().build();
        }
    }

    private BannerStats getStats(final long pageId)
    {
        AbstractPage page = pageManager.getAbstractPage(pageId);

        int likes = likeManager.countLikes(page);
        int comments = pageManager.getCommentCountOnPage(pageId);
        int versions = page.getLatestVersion().getVersion();

        return new BannerStats(likes, comments, versions);
    }
}