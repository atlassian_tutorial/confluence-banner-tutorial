# Tutorial: Adding items to the Info Banner

The plugin in this tutorial inserts a new item called "Some statistics" to the Info Banner. 
Clicking this item launches a dialog, which contains some basic information about the page 
(number of likes, comments and versions). From this starting point, it is possible to create 
more advanced plugins to show any type of customised information.

For the full tutorial, please see: [Adding items to the Info Banner][1].

## Running locally

To run this app locally, make sure that you have the [Atlassian Plugin SDK][2] installed, and then run:

    atlas-mvn confluence:run

 [1]: https://developer.atlassian.com/confdev/tutorials/adding-items-to-the-info-banner
 [2]: https://developer.atlassian.com/docs/getting-started/set-up-the-atlassian-plugin-sdk-and-build-a-project